## Contexto

Esta web sirve para redirigir a una de las webs que tenemos instaladas.
El sistema de sindominio prevee dos casos:

1. Mono-site, donde un repositorio `git.sindominio.net/colectivo/web`
   se monta en `https://sindominio.net/colectivo/`.
2. Multi-site, donde varios repositorios, como
  `git.sindominio.net/colectivo/web1` y
  `git.sindominio.net/colectivo/web2`,
  se montan en
  `https://sindominio.net/colectivo/web1/` y
  `https://sindominio.net/colectivo/web2`, respectivamente.

En el caso de que queramos tener una web "multisite", no está previsto montar nada con hugo en la raíz del colectivo, es decir, `https://sindominio.net/colectivo/` se queda vacía, o si hemos estado trasteando, con la última web monosite que le pusimos.

## Miniweb de redirección

Esta web es un site por defecto, con el tema por defecto Mainroad, modificado para redirigir a una URL configurable.

El objetivo es que sirva como puerta de entrada a las otras webs, quienes se pueden enlazar entre ellas.

