---
title: "…"
date: 2021-01-25
---

Si us plau, permet la redirecció o segueix l'enllaç a la [web principal](/logout/web/).

Por favor, permite la redirección o sigue el enlace hacia la [web principal](/logout/web/).

Please, allow the redirection or follow the link to the [main website](/logout/web/).
